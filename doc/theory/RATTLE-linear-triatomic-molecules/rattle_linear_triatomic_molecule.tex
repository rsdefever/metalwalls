\documentclass[12pt]{article} 
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{verbatim}
\usepackage{framed}
\usepackage{xcolor} 
\usepackage{graphicx} 
\usepackage[labelfont=bf]{caption,subcaption} \captionsetup[sub]{subrefformat=parens}
\usepackage{hyperref}\hypersetup{ colorlinks = true, linkcolor = blue }
\usepackage{fancyhdr}
\usepackage{amsmath}


\setlength{\textheight}{25cm}
\setlength{\textwidth}{17cm}
\setlength{\oddsidemargin}{-5mm }
\setlength{\topmargin}{-2.5cm}
\setlength{\evensidemargin}{3.0mm}
\setlength\parindent{0pt}

\usepackage[hyphenbreaks]{breakurl}
\pagestyle{fancy} 
\renewcommand{\headrulewidth}{0pt}  
\newcommand{\tunderbrace}[1]{\underbrace{\textstyle#1}}
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}
\makeatletter

\author{Thomas Dufils}
\title{RATTLE algorithm for linear triatomic molecules}
\date{}

\begin{document}
\setlength{\abovedisplayskip}{15pt}
\setlength{\belowdisplayskip}{15pt}
\setlength{\abovedisplayshortskip}{15pt}
\setlength{\belowdisplayshortskip}{15pt}
\setlength{\jot}{10pt}

\maketitle
In molecular dynamics simulations, intramolecular bond may be treated explicitly with a potential that reproduces the main features of the bond (bond length, vibration frequencies...). But the treatment of vibrations in particular may be even more challenging since it needs to develop a force field and may result in the need of a smaller timestep if the frequency of the mode is high (for instance the O-H bond in water). In cases when this level of description is not needed, constraint algorithm are used to keep the bonds rigid. The two main constraint algorithms used in MD simulations are SHAKE and RATTLE. When molecules (more than three sites) are subject to multiple constraints, such as water, these algorithms are relevant only if the molecule is not linear like carbon dioxide. Indeed in this case the system looses one rotational degree of freedom (along the axe of the molecule), leading to a number of constraints larger than the number of unknowns. An alternative would be to use other kinds of constraint algorithm, for instance using the quaternion framework. We present here a route to derive the equations of the RATTLE algorithm in the case of linear molecules.

\section{Presentation of the system and notations}
We consider a linear molecule with three sites referred as 1, 2 and 3 as shown on Figure \ref{molecule}. Each site $i$ has a position $\mathbf{r}_i$, a velocity $\mathbf{v}_i$, a resultant of the forces $\mathbf{f}_i$ and a mass $m_{i}$.


\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.4]{molecule.pdf}
\caption{Configuration of the molecule\label{molecule}}
\end{center}
\end{figure}


In this molecule the distance $r_{13}$ is constrained to the value $d$, the distances $r_{23}$ and $r_{12}$ being constrained to $\lambda d$ and $(1-\lambda)d$ respectively. In the case of a linear molecule, these constraints read 
\begin{align}
r_{13}^{2}-d^{2}=\sigma=0 \label{constr_dist_sig}\\
\mathbf{r}_2-(1-\lambda)\mathbf{r}_1-\lambda \mathbf{r}_3=\mathbf{\tau}=0 \label{constr_dist_tau}
\end{align}


This corresponds to four scalar equations instead of three, so the rotational degree of freedom along the axis of the molecule has been removed. By deriving these constraints with respect to time, we obtain the constrains on the velocity
\begin{eqnarray}
\mathbf{v}_{13}\cdot \mathbf{r}_{13}= \dot{\sigma} = 0 \label{constr_vel_sig} \\
\mathbf{v}_{2}-(1-\lambda)\mathbf{v}_{1}-\lambda \mathbf{v}_{3}= \dot{\mathbf{\tau}} = 0 \label{constr_vel_tau}
\end{eqnarray}

where $\mathbf{v}_{13}=\mathbf{v}_{3}-\mathbf{v}_{1}$ and $\mathbf{r}_{13}=\mathbf{r}_{3}-\mathbf{r}_{1}$

\section{General derivation}
In the equations of motion, these constraints will correspond to an effective force applied on atom $i$
\begin{equation}
	\mathbf{g}_{i}=-a\mathbf{\nabla}_{\mathbf{r}_{i}}\sigma- \mathbf{\nabla}_{\mathbf{r}_{i}}(\mathbf{b} \cdot \mathbf{\tau}),
\end{equation}

where $a$ and $\mathbf{b}$ are the Lagrange multipliers. If we expand this expression for each of the three atoms we obtain
\begin{equation}
\begin{alignedat}{1}
&\mathbf{g}_{1}=2a\mathbf{r}_{13}+(1-\lambda)\mathbf{b}\\
&\mathbf{g}_{2}=-\mathbf{b}\\
&\mathbf{g}_{3}=-2a\mathbf{r}_{13}+\lambda \mathbf{b}
\end{alignedat}
\end{equation}

We notice that the sum of the $\mathbf{g}_{i}$ is 0, meaning that the total momentum of the molecule is not modified by these additional terms. We can write the equations of motion
\begin{equation}
\begin{alignedat}{2}
&m_{1}\ddot{\mathbf{r}}_{1}=\mathbf{f}_{1}+2a\mathbf{r}_{13}+(1-\lambda) \mathbf{b}&=\mathbf{F}_{1}\\
&m_{2}\ddot{\mathbf{r}}_{2}=\mathbf{f}_{2}-\lambda \mathbf{b}&=\mathbf{F}_{2}\\
&m_{3}\ddot{\mathbf{r}}_{3}=\mathbf{f}_{3}-2a\mathbf{r}_{13}+\lambda \mathbf{b}&=\mathbf{F}_{3}
\label{eq_motion_init}
\end{alignedat}
\end{equation}


where $a$ and $\mathbf{b}$ are still unknown. In the following, we will only be interested in the motion of atoms 1 and 3 (referred as the bases) and determine the position and velocity of atom 2 by using the constraints (\ref{constr_dist_tau}) and (\ref{constr_vel_tau}). First, we shall use the second derivative of constraint (\ref{constr_dist_tau})
\begin{equation}
\ddot{\mathbf{\tau}}=0=\ddot{\mathbf{r}}_{2}-(1-\lambda)\ddot{\mathbf{r}}_{1} -\lambda \ddot{\mathbf{r}}_{3}
\end{equation}

By using the equations of motion (\ref{eq_motion_init}) we can write
\begin{equation}
\dfrac{1}{m_{2}}(\mathbf{f}_{2}-b)-\dfrac{1-\lambda}{m_{1}}(\mathbf{f}_{1}+2a\mathbf{r}_{13}+\lambda \mathbf{b})-\dfrac{\lambda}{m_{3}}(\mathbf{f}_{3}-2a\mathbf{r}_{13}+\lambda \mathbf{b})=0,
\end{equation}

where we can regroup the terms proportional to $\mathbf{b}$
\begin{equation}
\left[\dfrac{1}{m_{2}}+\dfrac{(1-\lambda)^{2}}{m_{1}}+\dfrac{\lambda^{2}}{m_{3}}\right]\mathbf{b}=\dfrac{1}{m_{2}}\mathbf{f}_{2}-\dfrac{1-\lambda}{m_{1}}\mathbf{f}_{1}-\dfrac{\lambda}{m_{3}}\mathbf{f}_{3}-2a\left(\dfrac{1-\lambda}{m_{1}}-\dfrac{\lambda}{m_{3}}\right) \mathbf{r}_{13}.
\end{equation}

To simplify this we pose $A=\dfrac{1}{m_{2}}+\dfrac{(1-\lambda)^{2}}{m_{1}}+\dfrac{\lambda^{2}}{m_{3}}$ and $B=\dfrac{1-\lambda}{m_{1}}-\dfrac{\lambda}{m_{3}}$. We now can express $\mathbf{b}$ as a function of $a$
\begin{equation}
\mathbf{b}=\dfrac{1}{Am_{2}}\mathbf{f}_{2} -\dfrac{1-\lambda}{Am_{1}}\mathbf{f}_{1} -\dfrac{\lambda}{Am_{3}}\mathbf{f}_{3} -\dfrac{2aB}{A}\mathbf{r}_{13}.
\end{equation}

Let us now replace $\mathbf{b}$ in the equations of motion (\ref{eq_motion_init}) by its expression
\begin{equation}
\begin{alignedat}{1}
&\ddot{\mathbf{r}}_{1}=\dfrac{1}{m_{1}}\mathbf{f}_{1} + \underbrace{\dfrac{1-\lambda}{Am_{2}m_{1}}\mathbf{f}_{2} - \dfrac{(1-\lambda)^{2}}{Am_{1}^{2}}\mathbf{f}_{1} - \dfrac{\lambda(1-\lambda)}{Am_{1}m_{3}}\mathbf{f}_{3}}_{\textstyle \dfrac{1}{m_{1}} \mathbf{f}_{correct,1}} - \underbrace{\dfrac{2a}{m_{1}}\left[\dfrac{(1-\lambda)B}{A}-1\right] \mathbf{r}_{13}}_{\textstyle \dfrac{G_{1}}{m_{1}}\mathbf{r}_{13}}\\
&\ddot{\mathbf{r}}_{3}=\dfrac{1}{m_{3}}\mathbf{f}_{3} + \underbrace{\dfrac{\lambda}{Am_{3}m_{2}}\mathbf{f}_{2} - \dfrac{\lambda(1-\lambda)}{Am_{1}m_{3}}\mathbf{f}_{1} - \dfrac{\lambda^{2}}{Am_{3}^{2}}\mathbf{f}_{3}}_{\textstyle \dfrac{1}{m_{3}} \mathbf{f}_{correct,3}} - \underbrace{\dfrac{2a}{m_{3}}\left[\dfrac{\lambda B}{A}+1\right] \mathbf{r}_{13}}_{\textstyle \dfrac{G_{3}}{m_{3}}\mathbf{r}_{13}} \label{eq_motion_developped}
\end{alignedat}
\end{equation}


These equations look similar to a diatomic molecule composed of the two bases with a constrained bond. We notice some differencies with the standard RATTLE or SHAKE algorithm:
\begin{itemize}
\item The forces on the two bases are corrected by the force apllied on each of the atoms of the molecule.
\item The constraints $G_{i}$ applied on each of the bases are not opposites.
\end{itemize}

Now we still have to find the last Lagrange multiplier $a$. For this we need to detail how the positions and velocities are updated at each timestep. The RATTLE algorithm is based on the velocity Verlet algorithm

\begin{align}
&\mathbf{r}(t+dt)=\mathbf{r}(t)+dt \mathbf{v}(t)+dt^{2}\dfrac{\mathbf{F}(t)}{2m}\label{update_r}\\
&\mathbf{v}(t+dt)=\mathbf{v}(t)+dt\dfrac{\mathbf{F}(t)+\mathbf{F}(t+dt)}{2m} \label{update_v}
\end{align}

\section{Update of positions}
Using the equations of motions (\ref{eq_motion_developped}) and the relation for the update of positions (\ref{update_r}) this leads to
\begin{equation}
\begin{alignedat}{1}
	&\mathbf{r}_{1}(t+dt)=\underbrace{\mathbf{r}_{1}(t)+dt\mathbf{v}_{1}(t)+\dfrac{dt^{2}}{2m_{1}}\mathbf{f}_{1}(t)+\dfrac{dt^{2}}{2m_{1}}\mathbf{f}_{correct,1}(t)}_{\textstyle \mathbf{r}_{1}^{0}(t+dt)} -\underbrace{\dfrac{dt^{2}}{2m_{1}}G_{1}(t)\mathbf{r}_{13}(t)}_{\textstyle a_{r}\dfrac{dt^{2}}{2}C_{1}\mathbf{r}_{13}(t)}\\
	&\mathbf{r}_{3}(t+dt)=\underbrace{\mathbf{r}_{3}(t)+dt\mathbf{v}_{3}(t)+\dfrac{dt^{2}}{2m_{3}}\mathbf{f}_{3}(t)+\dfrac{dt^{2}}{2m_{3}}\mathbf{f}_{correct,3}(t)}_{\textstyle \mathbf{r}_{3}^{0}(t+dt)} -\underbrace{\dfrac{dt^{2}}{2m_{3}}G_{3}(t)\mathbf{r}_{13}(t)}_{\textstyle a_{r}\dfrac{dt^{2}}{2}C_{3}\mathbf{r}_{13}(t)}
\end{alignedat}
\end{equation}


where $\mathbf{r}_{i}^{0}(t+dt)$ is our first guess of the position of the atom i at time $t+dt$ and $C_{1}=\dfrac{2}{m_{1}}\left[\dfrac{(1-\lambda)B}{A}-1\right]$ and $C_{3}=\dfrac{2}{m_{3}}\left[\dfrac{\lambda B}{A}+1\right]$ are constants. Now all the terms except for $a_{r}$ are known. We determine it by imposing the constraint (\ref{constr_dist_sig}) to be fulfilled at time $t+dt$. It reads
\begin{equation}
d^{2}=r_{13}(t+dt)^{2},
\end{equation}
or
\begin{equation}
	d^{2}=\Vert \mathbf{r}_{13}^{0}(t+dt) +a_{r}\dfrac{dt^{2}}{2}(C_{1}-C_{3})\mathbf{r}_{13}(t) \Vert^{2}.
\end{equation}

If we pose $\alpha=\dfrac{C_{1}-C_{3}}{2}=\dfrac{(1-\lambda)B}{Am_{1}}-\dfrac{1}{m_{1}}-\dfrac{B\lambda}{Am_{3}}-\dfrac{1}{m_{3}}$, we can develop it
\begin{equation}
	\left[dt^{4}r_{13}(t)^{2}\alpha^{2}\right]a_{r}^{2}+2\left[\alpha dt^{2}\mathbf{r^{0}}_{13}(t+dt) \cdot \mathbf{r}_{13}(t)\right]a_{r}+\left[r_{13}^{0}(t+dt)^{2}-d^{2}\right]=0
\end{equation}

Since $dt$ is small, $\mathbf{r}_{13}^{0}(t+dt)$ will not differ that much from $\mathbf{r}_{13}(t)$, leading to $\mathbf{r}_{13}^{0}(t+dt) \cdot \mathbf{r}_{13}(t)$ being always positive. This ensures that this second order equation has two solutions referred as $a_{r}^{\pm}$
\begin{equation}
	a_{r}^{\pm}=\dfrac{-\mathbf{r}_{13}^{0}(t+dt) \cdot \mathbf{r}_{13}(t) \pm \sqrt{\left[\mathbf{r}_{13}^{0}(t+dt) \cdot \mathbf{r}_{13}(t)\right]^{2} - r_{13}(t)^2\left[r_{13}^{0}(t+dt)^{2}-d^{2}\right]}}{\alpha dt^{2}r_{13}(t)^2}.
\end{equation}


Which one of these two solutions should we choose? The physically relevant solution is the one whose sign changes when the sign of $r_{13}^{0}(t+dt)^{2}-d^{2}$ changes and this one is $a_{r}^{+}$. Note that it corresponds to the solution leading to a correction of minimal norm. These two solutions have a geometric interpretation displayed on Figure \ref{racines}. When $a_{r}^{+}$ corresponds to a slight change of $\mathbf{r}_{13}^{0}(t+dt)$, $a_{r}^{-}$ strongly changes the orientation of the molecule, almost flipping it in the opposite direction.

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.4]{racines.pdf}
\caption{Geometric interpretation of $a_{r}^{+}$ and $a_{r}^{-}$\label{racines}}
\end{center}
\end{figure}

If we assume that $r_{13}^{0}(t+dt)-d$ is small compared to $d$, we can proceed a first order expansion of the squareroot term in $\dfrac{r_{13}^{0}(t+dt)-d}{d}$
\begin{equation}
	a_{r} \simeq -\dfrac{r_{13}^{0}(t+dt)^{2}-d^{2}}{2\alpha dt^{2} \mathbf{r}_{13}^{0}(t+dt) \cdot \mathbf{r}_{13}(t)}
\end{equation}

This does not differ much from the correction on a diatomic molecule, where $\alpha$ replaces $1/m_{1}+1/m_{3}$.% The procedure of the algorithm is summarized on figure \ref{shake}.

\section{Update of velocities} 
The difficulty in the update of velocities is that as shown on equation (\ref{update_v}) it needs the constraint on time $t+dt$ that needs the velocity at time $t+dt$. So instead of determining the Lagrange multiplier as $a_{r}(t+dt)$, associated with the constraint on positions, we determine it as $a_{v}(t)$, associated with the constraint on velocities. We can now write the equations of the update of the velocities using the equations of motion (\ref{eq_motion_developped}) and equation (\ref{update_v}) as
\begin{equation}
\begin{alignedat}{1}
	&\mathbf{v}_{1}(t+dt)=\underbrace{\mathbf{v}_{1}(t)+\dfrac{dt}{2m_{1}}\mathbf{F}_{1}(t) + \dfrac{dt}{2m_{1}}\mathbf{f}_{1}(t+dt) + \dfrac{dt}{2m_{1}}\mathbf{f}_{correct,1}(t+dt)}_{\textstyle \mathbf{v}_{1}^{0}}(t+dt) -\underbrace{\dfrac{dt}{2m_{1}}G_{1}(t+dt)\mathbf{r}_{13}(t+dt)}_{\textstyle a_{v}\dfrac{dt}{2}C_{1}\mathbf{r}_{13}(t+dt)}\\
	&\mathbf{v}_{3}(t+dt)=\underbrace{\mathbf{v}_{3}(t)+\dfrac{dt}{2m_{3}}\mathbf{F}_{3}(t) + \dfrac{dt}{2m_{3}}\mathbf{f}_{3}(t+dt) + \dfrac{dt}{2m_{3}}\mathbf{f}_{correct,3}(t+dt)}_{\textstyle \mathbf{v}_{3}^{0}}(t+dt) -\underbrace{\dfrac{dt}{2m_{3}}G_{3}(t+dt)\mathbf{r}_{13}(t+dt)}_{\textstyle a_{v}\dfrac{dt}{2}C_{3}\mathbf{r}_{13}(t+dt)}
\end{alignedat}
\end{equation}


where $\mathbf{v}_{i}^{0}(t+dt)$ is our first guess of the velocity of the atom $i$ at time $t+dt$ and $\mathbf{F}_{i}(t)$ has been calculated during the update of the positions. Note that this guess depends on $a_{r}$ determined previously. Now all the terms except for $a_{v}$ are known. We determine it as in the standard RATTLE algorithm by imposing the contraint (\ref{constr_vel_sig}) to be fulfilled at time $t+dt$
\begin{equation}
	\mathbf{v}_{13}(t+dt) \cdot \mathbf{r}_{13}(t+dt)=0
\end{equation}
or
\begin{equation}
	\left[\mathbf{v}_{13}^{0}(t+dt)+\dfrac{a_{v}}{2}dt(C_{1}-C_{3}) \mathbf{r}_{13}(t+dt)\right] \cdot \mathbf{r}_{13}(t+dt)=0
\end{equation}

where we once again recognize $\alpha$. This is a first order equation in $a_{v}$ that is easy to solve to find
\begin{equation}
	a_{v}=-\dfrac{\mathbf{v}_{13}^{0}(t+dt) \cdot \mathbf{r}_{13}(t+dt)}{\alpha r_{13}^{2}(t+dt) dt}.
\end{equation}

As for the update of positions, we find a similar result to a diatomic molecule with a bond constraint where $\alpha$ replaces $1/m_{1}+1/m_{3}$. These similarities make this algorithm easy to implement in MD codes where the standard RATTLE algorithm is already implemented. The procedure of the algorithm is summarized on Figure \ref{rattle}.

\begin{figure}[!h]
\begin{center}
\includegraphics[scale=0.6]{rattle-algorithm.pdf}
\caption{Molecular dynamics with the RATTLE algorithm\label{rattle}}
\end{center}
\end{figure}



%\section{Annexe}
%\subsection{Sum up shake algorithm}

%\setlength\parindent{24pt}
%\subsubsection*{$\mathbf{r}(t), \mathbf{v}(t)$}
%\subsubsection*{calculate the forces at time $t$}
%\subsubsection*{update the positions and velocities}
%\hspace*{24pt}determine $\mathbf{f_{correct}}(t)$\\

%\hspace*{48pt}deduce $\mathbf{r^{0}}(t)$\\

%\hspace*{48pt}update $v(t)$ with $\mathbf{f_{correct}}(t)$\\

%determine $a_{r}$\\

%update the positions and velocities
%\subsubsection*{$\mathbf{r}(t+dt), \mathbf{v}(t+dt)$}


%\subsection{Sum up rattle algorithm}
%\setlength\parindent{24pt}
%\subsubsection*{$\mathbf{r}(t), \mathbf{v}(t)$}
%\subsubsection*{calculate the forces at time $t$}
%\subsubsection*{update the positions}
%\hspace*{24pt}determine $\mathbf{f_{correct}}(t)$\\

%\hspace*{48pt}deduce $\mathbf{r^{0}}(t)$\\

%\hspace*{48pt}update $v(t)$ with $\mathbf{f_{correct}}(t)$\\

%determine $a_{r}$\\

%update the positions and velocities (contribution of position constraint at time $t$)
%\subsubsection*{calculate the forces at time $t+dt$}
%\subsubsection*{update the velocities}
%\hspace*{24pt}determine $\mathbf{f_{correct}}(t+dt)$\\

%\hspace*{48pt}deduce $\mathbf{v^{0}}(t)$\\

%determine $a_{v}$\\

%update the velocities (contribution of velocity constraint at time $t$)
%\subsubsection*{$\mathbf{r}(t+dt), \mathbf{v}(t+dt)$}

\end{document}
