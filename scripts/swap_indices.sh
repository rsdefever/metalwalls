#! /usr/local/bin/bash
# Sed script to efficiently swap indices order for some vectors
# It can be useful to test the efficiency of some data layout against another one
SRC_DIR=../src
#TEST_DIR=${SRC_DIR}/tests/

# Swap index of dimensions with index of particle
ARRAYS="xyz_elec xyz_ions force velocity_ions forces_ions xyz_now xyz_next velocity_next forces forces_ref xyz_atoms"
#ARRAYS="xyz_atoms"
#SRC_FILES="coulomb.f90 lennard_jones.f90 system.f90 ewald.f90 rattle.f90"
#TEST_FILES="test_rattle.f90 test_coulomb.f90 test_ewald.f90"
for f in ${SRC_DIR}/*
do
    cp ${SRC_DIR}/${f} tmp1
    for a in ${ARRAYS}
    do
        sed -e "s/${a}(\([:0-9a-zA-Z_][%0-9a-zA-Z_]*\),\([:0-9a-zA-Z_][%0-9a-zA-Z_]*\)/${a}(\2,\1/g" \
            -e "s/size(${a},1)/size(${a},2)/g" \
            -e "s/size(${a},2)/size(${a},1)/g" \
            < tmp1 > tmp2
        mv tmp2 tmp1
    done
    mv tmp1 ${f}
done

# for f in ${TEST_FILES}
# do
#     cp ${TEST_DIR}/${f} tmp1
#     for a in ${ARRAYS}
#     do
#         sed "s/${a}(\([:0-9a-zA-Z_][%0-9a-zA-Z_]*\),\([:0-9a-zA-Z_][%0-9a-zA-Z_]*\)/${a}(\2,\1/g" < tmp1 > tmp2
#         mv tmp2 tmp1
#     done
#     mv tmp1 ${f}
# done

