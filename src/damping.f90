!> Ion-dipole damping potential
!! -------------------
!!
!!
module MW_damping
   use MW_kinds, only: wp
   implicit none
   private

   ! Public type
   ! -----------
   public :: MW_damping_t

   ! Public subroutines
   ! ------------------
   public :: define_type
   public :: void_type
   public :: print_type

   type MW_damping_t
      integer :: num_species = 0                            ! number of species
      real(wp), dimension(:,:), allocatable :: b            ! (num_species,num_species) inverse of range of the damping
      integer, dimension(:,:), allocatable :: order         ! (num_species,num_species) order of the Tang-Toennies function
      real(wp), dimension(:,:), allocatable :: c            ! (num_species,num_species) intensity of the damping
   end type MW_damping_t

contains

   !================================================================================
   ! Define the data structure
   subroutine define_type(this, num_species)
      use MW_errors, only: &
            MW_errors_allocate_error => allocate_error, &
            MW_errors_parameter_error => parameter_error
      implicit none
      ! Parameters
      ! ---------_
      type(MW_damping_t), intent(inout) :: this      !> structure to be defined
      integer, intent(in) :: num_species                    !> number of species

      ! Local
      ! -----
      integer :: ierr

      if (num_species <= 0) then
         call MW_errors_parameter_error("define_type", "damping.f90",&
               "num_species", num_species)
      end if

      this%num_species = num_species

      allocate(this%b(num_species,num_species), &
            this%order(num_species,num_species), &
            this%c(num_species,num_species), stat=ierr)
      if (ierr /= 0) &
            call MW_errors_allocate_error("define_type", "damping.f90", ierr)
      this%b(:,:) = 0.0_wp
      this%order(:,:) = 0
      this%c(:,:) = 0.0_wp

   end subroutine define_type

   !================================================================================
   ! Void the data structure
   subroutine void_type(this)
      use MW_errors, only: MW_errors_deallocate_error => deallocate_error
      implicit none
      type(MW_damping_t), intent(inout) :: this

      integer :: ierr

      if (allocated(this%b)) then
         deallocate(this%b, this%order, this%c, stat=ierr)
         if (ierr /= 0) &
               call MW_errors_deallocate_error("define_type", "damping.f90", ierr)
      end if

      this%num_species = 0
   end subroutine void_type


   !================================================================================
   ! Print the data structure to ounit
   subroutine print_type(this, species_names, ounit)
      use MW_kinds, only: PARTICLE_NAME_LEN
      implicit none
      type(MW_damping_t), intent(in) :: this               ! structure to be printed
      character(PARTICLE_NAME_LEN),   intent(in) :: species_names(:)   ! names of the species
      integer,                   intent(in) :: ounit              ! output unit

      integer :: i, j

      write(ounit, '("|damping| ",8x,1x,8x,1x,"    b       ",1x," order ", &
            &"     c      ")')

      do j = 1, this%num_species
         do i = 1, this%num_species
            write(ounit, '("|damping| ",a8,1x,a8,1x,es12.5,1x,i1,1x,es12.5)') &
                  species_names(i), species_names(j), this%b(i,j), this%order(i,j),&
                  this%c(i,j)
         end do
      end do

   end subroutine print_type

end module MW_damping
