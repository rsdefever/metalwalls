mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))
current_build_dir := $(build_dir)/$(current_dir)

#$(info MAKEFILE_LIST = $(MAKEFILE_LIST))
#$(info current_dir = $(current_dir))
#$(info current_build_dir = $(current_build_dir))

srcs_dir += $(current_dir)
local_srcs_f90 := $(wildcard $(current_dir)/*.f90)
local_srcs_F90 := $(wildcard $(current_dir)/*.F90)
local_srcs_inc := $(wildcard $(current_dir)/*.inc)

# Delete every generated 2DPBC and 3DPBC inc files except the specific ones that are not generated
local_srcs_inc_to_delete := $(wildcard $(current_dir)/*2DPBC.inc)
local_srcs_inc_to_delete += $(wildcard $(current_dir)/*3DPBC.inc)
local_srcs_inc_to_delete := $(filter-out %minimum_image_displacement_2DPBC.inc,$(local_srcs_inc_to_delete))
local_srcs_inc_to_delete := $(filter-out %minimum_image_displacement_3DPBC.inc,$(local_srcs_inc_to_delete))
local_srcs_inc_to_delete += $(wildcard $(current_dir)/version*.inc)


# ================================================================================
srcs_f90 += $(local_srcs_f90)
srcs_F90 += $(local_srcs_F90)
srcs_inc += $(local_srcs_inc)
srcs_inc_to_delete += $(local_srcs_inc_to_delete)
