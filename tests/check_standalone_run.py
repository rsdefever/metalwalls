"""
check_standalone_run.py

This script validate MW2 output reference already present
"""
import mwrun

fails = 0

def assertTrue(ok, msg, quantity):
  if not ok:
    print "Error in %s:"%quantity
    print msg
    fails += 1

def check():
  n = mwrun.mwrun(None, ".")

  ok, msg = n.compare_datafiles("forces.out", "forces.ref", rtol=1.0E-7, atol=0.0)
  assertTrue(ok, msg, "forces")

  ok, msg = n.compare_datafiles("energies.out", "energies.ref", rtol=1.0E-7, atol=0.0)
  assertTrue(ok, msg, "energies")

  ok, msg = n.compare_datafiles("energies_breakdown.out", "energies_breakdown.ref", rtol=1.0E-7, atol=0.0)
  assertTrue(ok, msg, "energies_break_down")
  
  ok, msg = n.compare_datafiles("charges.out", "charges.ref", rtol=1.0E-7, atol=0.0)
  assertTrue(ok, msg, "electrodes")

if __name__ == "__main__":
  check()
  if fails == 0:
    print "All tests successfull"
  else:
    print "%d tests failed"%fails
