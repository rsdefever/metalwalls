LAMMPS (12 Dec 2018)
units               real
dimension           3
newton              on
boundary            p p f
atom_style          full

#  Atom definition
read_data capacitor.lmp
  orthogonal box = (-9.838 -8.52 -140.2) to (9.838 8.52 140.2)
  1 by 1 by 1 MPI processor grid
  reading atoms ...
  1728 atoms
  0 = max # of 1-2 neighbors
  0 = max # of 1-3 neighbors
  0 = max # of 1-4 neighbors
  1 = max # of special neighbors

variable CAB equal 1
variable CAC equal 2
variable CG equal 3
variable NAA equal 4

# Group
group electrode1 molecule 81
384 atoms in group electrode1
group electrode2 molecule 82
384 atoms in group electrode2
group electrode type ${CG}
group electrode type 3
768 atoms in group electrode
group mobile type ${CAB} ${CAC} ${NAA}
group mobile type 1 ${CAC} ${NAA}
group mobile type 1 2 ${NAA}
group mobile type 1 2 4
960 atoms in group mobile

set type ${CAB} charge 0.129
set type 1 charge 0.129
  320 settings made for charge
set type ${CAC} charge 0.269
set type 2 charge 0.269
  320 settings made for charge
set type ${NAA} charge -0.398
set type 4 charge -0.398
  320 settings made for charge
set group electrode1 charge 0.01
  384 settings made for charge
set group electrode2 charge -0.01
  384 settings made for charge

# Force field
pair_style lj/cut/coul/long 8.0
kspace_style ewald 1.0e-8
kspace_modify slab 4.0
pair_modify shift yes

pair_coeff ${CAB}   ${CAB}      0.09935850       3.40000000  # CAB CAB
pair_coeff 1   ${CAB}      0.09935850       3.40000000  
pair_coeff 1   1      0.09935850       3.40000000  
pair_coeff ${CAB}   ${CAC}      0.19419428       3.50000000  # CAB CAC
pair_coeff 1   ${CAC}      0.19419428       3.50000000  
pair_coeff 1   2      0.19419428       3.50000000  
pair_coeff ${CAB}   ${CG}       0.07390356       3.38500000  # CAB CG
pair_coeff 1   ${CG}       0.07390356       3.38500000  
pair_coeff 1   3       0.07390356       3.38500000  
pair_coeff ${CAB}   ${NAA}      0.09935850       3.35000000  # CAB NAA
pair_coeff 1   ${NAA}      0.09935850       3.35000000  
pair_coeff 1   4      0.09935850       3.35000000  
pair_coeff ${CAC}   ${CAC}      0.37954900       3.60000000  # CAC CAC
pair_coeff 2   ${CAC}      0.37954900       3.60000000  
pair_coeff 2   2      0.37954900       3.60000000  
pair_coeff ${CAC}   ${CG}       0.14444310       3.48500000  # CAC CG
pair_coeff 2   ${CG}       0.14444310       3.48500000  
pair_coeff 2   3       0.14444310       3.48500000  
pair_coeff ${CAC}   ${NAA}      0.19419428       3.45000000  # CAC NAA
pair_coeff 2   ${NAA}      0.19419428       3.45000000  
pair_coeff 2   4      0.19419428       3.45000000  
pair_coeff ${CG}    ${CG}       0.0 1.0 #0.05497000       3.37000000  # CG CG
pair_coeff 3    ${CG}       0.0 1.0 
pair_coeff 3    3       0.0 1.0 
pair_coeff ${CG}    ${NAA}      0.07390356       3.33500000  # CG NAA
pair_coeff 3    ${NAA}      0.07390356       3.33500000  
pair_coeff 3    4      0.07390356       3.33500000  
pair_coeff ${NAA}   ${NAA}      0.09935850       3.30000000  # NAA NAA
pair_coeff 4   ${NAA}      0.09935850       3.30000000  
pair_coeff 4   4      0.09935850       3.30000000  
pair_coeff * * 0.0 1.0

write_data ff.dat pair ij
Ewald initialization ...
  using 12-bit tables for long-range coulomb (../kspace.cpp:321)
  G vector (1/distance) = 0.450585
  estimated absolute RMS force accuracy = 2.68912e-06
  estimated relative force accuracy = 8.0982e-09
  KSpace vectors: actual max1d max3d = 140097 464 400882544
                  kxmax kymax kzmax  = 11 10 464
Neighbor list info ...
  update every 1 steps, delay 10 steps, check yes
  max neighbors/atom: 2000, page size: 100000
  master list distance cutoff = 10
  ghost atom cutoff = 10
  binsize = 5, bins = 4 4 57
  1 neighbor lists, perpetual/occasional/extra = 1 0 0
  (1) pair lj/cut/coul/long, perpetual
      attributes: half, newton on
      pair build: half/bin/newton
      stencil: half/bin/3d/newton
      bin: standard
neigh_modify check yes delay 0 every 1

#  Fixes, thermostats and barostats / Run
timestep 1

dump out1 all custom 1 traj.lammpstrj id element x y z fx fy fz
dump_modify out1 element CAB CAC CG NAA sort id format float %20.15g

dump out mobile custom 1 forces.ref id element x y z fx fy fz
dump_modify out element CAB CAC CG NAA sort id format float %20.15g

dump out2 electrode custom 1 elec_forces.ref id element fx fy fz
dump_modify out2 element CAB CAC CG NAA sort id format float %20.15g

velocity all set 0.0 0.0 0.0

thermo_style custom step temp epair emol ke pe etotal cella cellb cellc
thermo 100

compute fz1 electrode1 reduce sum fz
compute fz2 electrode2 reduce sum fz
compute fy1 electrode1 reduce sum fy
compute fy2 electrode2 reduce sum fy
compute fx1 electrode1 reduce sum fx
compute fx2 electrode2 reduce sum fx

#variable nc equal count(electrode1)
#variable surface equal cella*cellb
#variable pressure equal 0.0
#variable conversion equal 101325*6.02214076*(10^-10)/4.184
#variable fp1 equal ${pressure}*${conversion}*${surface}
#variable fp2 equal -${pressure}*${conversion}*${surface}

#variable f1 equal ${fp1}/${nc}+c_fz1/${nc}
#variable f2 equal ${fp2}/${nc}+c_fz2/${nc}
#variable f1 equal ${fp1}+c_fz1
#variable f2 equal ${fp2}+c_fz2
variable f1z equal c_fz1
variable f2z equal c_fz2
variable f1y equal c_fy1
variable f2y equal c_fy2
variable f1x equal c_fx1
variable f2x equal c_fx2

fix drag1 electrode1 setforce v_f1x v_f1y v_f1z
fix drag2 electrode2 setforce v_f2x v_f2y v_f2z

fix integrator all nve

run 0
Ewald initialization ...
  using 12-bit tables for long-range coulomb (../kspace.cpp:321)
  G vector (1/distance) = 0.450585
  estimated absolute RMS force accuracy = 2.68912e-06
  estimated relative force accuracy = 8.0982e-09
  KSpace vectors: actual max1d max3d = 140097 464 400882544
                  kxmax kymax kzmax  = 11 10 464
Per MPI rank memory allocation (min/avg/max) = -8.796e+12 | -8.796e+12 | -8.796e+12 Mbytes
Step Temp E_pair E_mol KinEng PotEng TotEng Cella Cellb Cellc 
       0            0   -72.165953            0            0   -72.165953   -72.165953       19.676        17.04        280.4 
Loop time of 1.90735e-06 on 1 procs for 0 steps with 1728 atoms

104.9% CPU use with 1 MPI tasks x no OpenMP threads

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Pair    | 0          | 0          | 0          |   0.0 |  0.00
Bond    | 0          | 0          | 0          |   0.0 |  0.00
Kspace  | 0          | 0          | 0          |   0.0 |  0.00
Neigh   | 0          | 0          | 0          |   0.0 |  0.00
Comm    | 0          | 0          | 0          |   0.0 |  0.00
Output  | 0          | 0          | 0          |   0.0 |  0.00
Modify  | 0          | 0          | 0          |   0.0 |  0.00
Other   |            | 1.907e-06  |            |       |100.00

Nlocal:    1728 ave 1728 max 1728 min
Histogram: 1 0 0 0 0 0 0 0 0 0
Nghost:    5712 ave 5712 max 5712 min
Histogram: 1 0 0 0 0 0 0 0 0 0
Neighs:    230772 ave 230772 max 230772 min
Histogram: 1 0 0 0 0 0 0 0 0 0

Total # of neighbors = 230772
Ave neighs/atom = 133.549
Ave special neighs/atom = 0
Neighbor list builds = 0
Dangerous builds = 0
Total wall time: 0:00:03
